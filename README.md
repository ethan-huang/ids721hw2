# Lambda Function for Finding Even Numbers from a CSV File

[Demo](https://z2pqn2tjg3.execute-api.us-east-1.amazonaws.com/default/findeven)
![Example Image](./images/4.png)

# Overview

This project demonstrates a Lambda function deployed on AWS that retrieves a CSV file from an S3 bucket, processes its contents to find even numbers, and returns the result.

# Features

- Retrieves a CSV file from an S3 bucket.
- Processes the CSV data to find even numbers.
- Returns the list of even numbers found in the CSV file.

# Prerequisites

- AWS account with appropriate permissions to create and manage Lambda functions and S3 buckets.
- Rust programming language installed locally.
- AWS CLI configured with appropriate IAM credentials.

Configure an S3 bucket event trigger to invoke the Lambda function whenever a CSV file is uploaded.

Test the Lambda function by uploading a CSV file containing numbers to the configured S3 bucket.

# Environment Variables

The Lambda function requires the following environment variables:

-BUCKET_NAME: The name of the S3 bucket where CSV files are stored.

-FILES_NAME: The name of the numbers file

# Screenshots:

![Example Image](./images/1.png)
![Example Image](./images/2.png)
![Example Image](./images/3.png)
